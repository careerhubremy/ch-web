module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        chBlue: '#0AA1C2',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
