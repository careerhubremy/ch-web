import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import Background from './Background/Background';
import styles from './styles.module.scss';
const LandingPage = ({}) => {
  const history = useHistory();
  const handleRegisterRedirect = () => {
    history.push({ pathname: '/auth', state: { form: 'Register' } });
  };
  const handleLoginRedirect = () => {
    history.push({ pathname: '/auth', state: { form: 'Login' } });
  };
  return (
    <div className={styles['root']}>
      <Background />
      <div className={styles['content']}>
        <h1 className={styles['title']}>CareerHub</h1>
        <div className={styles['actions']}>
          <button
            className={styles['register']}
            type="button"
            onClick={handleRegisterRedirect}
          >
            Зареєструватися
          </button>
          <button
            className={styles['login']}
            type="button"
            onClick={handleLoginRedirect}
          >
            Увійти
          </button>
        </div>
      </div>
    </div>
  );
};
export default LandingPage;
