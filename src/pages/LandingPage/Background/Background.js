import styles from './styles.module.scss';
import Ovals from './Ovals';
const Background = () => {
  return (
    <>
      <div className={styles['root']}>
        <Ovals />
        <div className={styles['pink-circle-left']} />
        <div className={styles['blue-circle-left']} />
        <div className={styles['light-blue-rect-left']} />
        <div className={styles['green-polygon-left']} />

        <div className={styles['pink-circle-right']} />
        <div className={styles['blue-circle-right']} />
        <div className={styles['light-blue-rect-right']} />
        <div className={styles['green-polygon-right']} />
      </div>
    </>
  );
};
export default Background;
