import { useEffect } from 'react';
import styles from './styles.module.scss';
const width = document.documentElement.clientWidth;
const height = document.documentElement.clientHeight;

const paint = (canvas) => {
  const shiftStep = 20;
  const radiusStep = 50;
  canvas.strokeStyle = 'rgba(255,255,255,1)';
  canvas.lineWidth = 3;

  for (let i = 0; i < 11; i += 1) {
    canvas.beginPath();
    canvas.ellipse(
      i * shiftStep,
      i * shiftStep,
      50 + i * radiusStep,
      50 + i * radiusStep,
      0,
      0,
      2 * Math.PI
    );
    canvas.stroke();
  }

  if (window.matchMedia('screen and (min-width: 768px)').matches) {
    for (let i = 0; i < 11; i += 1) {
      canvas.beginPath();
      canvas.ellipse(
        width - i * shiftStep,
        height - i * shiftStep,
        50 + i * radiusStep,
        50 + i * radiusStep,
        0,
        0,
        2 * Math.PI
      );
      canvas.stroke();
    }
  }
};

const Ovals = () => {
  useEffect(() => {
    paint(document.getElementsByTagName('canvas')[0].getContext('2d'));
  }, []);

  return <canvas width={width} height={height} />;
};

export default Ovals;
