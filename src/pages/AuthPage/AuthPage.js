import { useState } from 'react';
import { useLocation } from 'react-router-dom/cjs/react-router-dom.min';
import LoginForm from '../../components/Auth/LoginForm';
import RegisterForm from '../../components/Auth/RegisterForm';
import { emailLogin, emailRegiser } from '../../services/AuthService';
import Background from '../LandingPage/Background/Background';
import styles from './authPage.module.scss';

const AuthPage = () => {
  //todo state for SignUp vs signin
  const location = useLocation();
  const form = location?.state?.form;
  const [shownForm, setShownForm] = useState(
    form === undefined ? 'Register' : form
  );
  const handleFormChange = () => {
    setShownForm((prevState) =>
      prevState === 'Register' ? 'Login' : 'Register'
    );
  };
  const handleRegisterSubmit = (formData) => emailRegiser(formData);
  const handleLoginSubmit = (formData) => emailLogin(formData);

  return (
    <div className={styles['root']}>
      <Background />
      <div className={styles['form']}>
        {shownForm === 'Register' ? (
          <RegisterForm
            onFormSubmit={handleRegisterSubmit}
            onFormChange={handleFormChange}
          />
        ) : (
          <LoginForm
            onFormSubmit={handleLoginSubmit}
            onFormChange={handleFormChange}
          />
        )}
      </div>
    </div>
  );
};
export default AuthPage;
