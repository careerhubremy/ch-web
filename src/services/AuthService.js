import axios from 'axios';

const gateway = axios.create({
  baseURL: 'https://localhost:44307/api/auth',
  headers: {
    'Content-Type': 'application/json',
    // 'Access-Control-Allow-Origin': '*',
  }
});
export const emailLogin = ({ email, password }) => {
  gateway.post('/login',{email:email.value,password: password.value})
  .catch(error=>{
    console.error(error)
  })
  .then(response=>{
    console.log(response)
  })
};

export const emailRegiser = ({ email, password }) => {
  gateway.post('/register/student',{studentEmail:email.value,studentPassword:password.value})
  .catch(error=>{
    console.error(error)
  })
  .then(response=>{
    console.log(response)
  })
};
