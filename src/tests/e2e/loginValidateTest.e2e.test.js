describe("LoginTest", function () {

    let testInputValues = [
        {
            email: 'abcd@nure.ua',
            password: 'abcdABCD1234',
            emailExpectation: 'be.hidden',
            passwordExpectation: 'be.hidden'
        },
        {
            email: 'abcd@gmail.ua',
            password: 'abcdABCD1234@@',
            emailExpectation: 'be.hidden',
            passwordExpectation: 'be.hidden'
        },
        {
            email: 'abcd@nure.ua',
            password: 'abcdABC',
            emailExpectation: 'be.hidden',
            passwordExpectation: 'be.visible'},
        {
            email: 'abcd@gmail.ua',
            password: 'abcdABC',
            emailExpectation: 'be.hidden',
            passwordExpectation: 'be.visible'},
        {
            email: 'abcd@nure...ua',
            password: 'abcdABCD1234@@',
            emailExpectation: 'be.visible',
            passwordExpectation: 'be.hidden'
        },
        {
            email: 'выфвabcd@nure.ua',
            password: 'abcdABCD1234@@',
            emailExpectation: 'be.visible',
            passwordExpectation: 'be.hidden'
        },
        {
            email: 'abcd@nure.фів',
            password: 'abcdABCD1234@@',
            emailExpectation: 'be.visible',
            passwordExpectation: 'be.hidden'
        },
        {
            email: 'abcd@@nure.ua',
            password: '@@',
            emailExpectation: 'be.visible',
            passwordExpectation: 'be.visible'},
    ];

    testInputValues.forEach((item, index) => {
        it(`Check validation ${index}`, function () {
            cy.visit("http://localhost:3000/auth")
            cy.get('button[id="formSwitchButton"]').click();
            cy.get('input[type="email"]').type(item.email);
            cy.get('input[type="password"]').type(item.password);
            cy.get('label[id="emailValidationLabel"]').should(item.emailExpectation);
            cy.get('label[id="passwordValidationLabel"]').should(item.passwordExpectation);
        })
    });
})




