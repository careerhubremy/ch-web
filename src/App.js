import logo from './logo.svg';
import { Switch, Route } from 'react-router';
import LandingPage from './pages/LandingPage/LandingPage';
import './App.css';
import AuthPage from './pages/AuthPage/AuthPage';
import Navbar from './components/UI/Navbar/Navbar.js';

function App() {
  return (
    <>
      <Switch>
        <Route>
          <>
            <Switch>
              <Route path="/" exact component={LandingPage} />
              <Route path="/auth" exact component={AuthPage} />
            </Switch>
          </>
        </Route>
      </Switch>
    </>
  );
}
export default App;
