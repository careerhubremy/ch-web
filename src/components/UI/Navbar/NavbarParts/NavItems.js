export const NAV_ITEMS = [
  {
    title: 'Компанії',
    url: '#',
    clsName: 'nav-links',
    id: 'companiesLink',
  },
  {
    title: 'Робота',
    url: '#',
    clsName: 'nav-links',
    id: 'offersLink',
  },
  {
    title: 'Резюме',
    url: '#',
    clsName: 'nav-links',
    id: 'cvLink',
  },
];
