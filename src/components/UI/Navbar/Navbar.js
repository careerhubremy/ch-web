import { useState } from 'react';

import Button from '../Button';
import { NAV_ITEMS } from './NavbarParts/NavItems';
import styles from './navbar.module.scss';

const Navbar = (props) => {
  const [isBarsClicked, setIsBarsClicked] = useState(false);

  const barsClickHandler = () => {
    setIsBarsClicked(!isBarsClicked);
  };

  const navItems = NAV_ITEMS.map((item, index) => {
    return (
      <li key={index} id={item.id}>
        <a className={styles[item.clsName]} href={item.url}>
          {item.title}
        </a>
      </li>
    );
  });

  return (
    <nav id="navbar" className={styles['nav-main']}>
      <div id="navLogo" className={styles['nav-logo']}>
        <h2 id="navTitle">CareerHub</h2>
      </div>
      <div id="navIcon" className={styles['nav-icon']}>
        <i
          className={isBarsClicked ? 'fas fa-times' : 'fas fa-bars'}
          onClick={barsClickHandler}
        />
      </div>
      <ul
        id="navItemsList"
        className={styles[`nav-menu${isBarsClicked ? ' active' : ''}`]}
      >
        {navItems}
      </ul>
      <div className={styles['nav-user-menu']}>
        <Button id="logoutButton" className="button-dark">
          Вийти
        </Button>
      </div>
    </nav>
  );
};

export default Navbar;
