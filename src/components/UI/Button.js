import styles from './button.module.scss';

const Button = ({ children, type, onClick, className }) => {
  return (
    <button
      className={`${styles[className]} ${className}`}
      type={type || 'button'}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
