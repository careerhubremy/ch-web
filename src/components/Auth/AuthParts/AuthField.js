import React, { useState, useRef, useImperativeHandle } from 'react';

import styles from './styles.module.scss';
import cn from 'classnames';

const AuthField = React.forwardRef(
  (
    {
      id,
      type = 'text',
      placeholder = 'placeholder',
      children,
      isValid = true,
      onInputChange,
      onBlur,
      isPassword = false,
      validationMessage = 'Validation',
    },
    ref
  ) => {
    const inputRef = useRef();

    const [inputType, setInputType] = useState(type);

    const focusInput = () => {
      inputRef.current.focus();
    };

    useImperativeHandle(ref, () => {
      return {
        focus: focusInput,
      };
    });

    const showPasswordHandler = () => {
      setInputType((prevState) =>
        prevState === 'password' ? 'text' : 'password'
      );
    };

    return (
      <div id={`${id}FieldOuterDiv`} className={styles['root']}>
        <div
          id={`${id}FieldInnerDiv`}
          className={`${styles['auth-field']} ${!isValid && styles['invalid']}`}
        >
          <label
            id={`${id}FieldLabel`}
            htmlFor={id}
            className={styles['auth-field-label']}
          >
            {children}
          </label>
          <input
            id={`${id}FieldInput`}
            ref={inputRef}
            className={styles['auth-field-input']}
            type={inputType}
            placeholder={placeholder}
            onChange={onInputChange}
            onBlur={onBlur}
          />
          <button
            id={`${id}FieldButton`}
            type="button"
            onClick={showPasswordHandler}
            className={cn(
              styles['auth-field-eye'],
              !isPassword && styles['hidden']
            )}
          >
            {inputType === 'password' ? (
              <i className="far fa-eye-slash" />
            ) : (
              <i className="far fa-eye"></i>
            )}
          </button>
        </div>
        <label
          id={`${id}ValidationLabel`}
          className={cn(styles['validation'], isValid && styles['hidden'])}
        >
          {validationMessage}
        </label>
      </div>
    );
  }
);

export default AuthField;
