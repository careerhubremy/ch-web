import { useState, useEffect, useRef, useReducer } from 'react';

import AuthField from './AuthParts/AuthField';
import styles from './auth.module.scss';

const LoginForm = ({ onFormSubmit, onFormChange }) => {
  const emailRegex = /^[\w-\.]+@[\w-]+\.{1}[\w-]{2,}$/;
  const passwordRegex =
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9#?!@$%^&*-]).{8,32}$/;

  const emailReducer = (state, action) => {
    if (action.type === 'USER_INPUT') {
      return {
        value: action.val,
        isValid: action.val.match(emailRegex) || !action.val?.length,
      };
    }
    if (action.type === 'INPUT_BLUR') {
      return {
        value: state.value,
        isValid: state.value.match(emailRegex) || !state.value?.length,
      };
    }
    return { value: '', isValid: false };
  };

  const passwordReducer = (state, action) => {
    if (action.type === 'USER_INPUT') {
      return {
        value: action.val,
        isValid: action.val.match(passwordRegex) || !action.val?.length,
      };
    }
    if (action.type === 'INPUT_BLUR') {
      return {
        value: state.value,
        isValid: state.value.match(passwordRegex) || !state.value?.length,
      };
    }
    return { value: '', isValid: false };
  };

  const [formIsValid, setFormIsValid] = useState(false);

  const [emailState, dispatchEmail] = useReducer(emailReducer, {
    value: '',
    isValid: true,
  });

  const [passwordState, dispatchPassword] = useReducer(passwordReducer, {
    value: '',
    isValid: true,
  });

  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  const { isValid: emailIsValid } = emailState;
  const { isValid: passwordIsValid } = passwordState;

  useEffect(() => {
    const timer = setTimeout(() => {
      //DEBUG INFO
      console.log('CHECKING IF VALID...');
      setFormIsValid(emailIsValid && passwordIsValid);
    }, 500);

    return () => {
      clearTimeout(timer);
    };
  }, [emailIsValid, passwordIsValid]);

  const emailChangeHandler = (event) => {
    dispatchEmail({ type: 'USER_INPUT', val: event.target.value });
  };

  const passwordChangeHandler = (event) => {
    dispatchPassword({ type: 'USER_INPUT', val: event.target.value });
  };

  const validateEmailHandler = () => {
    dispatchEmail({ type: 'INPUT_BLUR' });
  };

  const validatePasswordHandler = () => {
    dispatchPassword({ type: 'INPUT_BLUR' });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    if (formIsValid) {
      // todo: implement authService
      onFormSubmit && onFormSubmit({
        email: emailState,
        password: passwordState
      });
      // authService.onRegister(emaiState.value, passwordState.value);
    } else if (!emailIsValid) {
      emailInputRef.current.focus();
    } else {
      passwordInputRef.current.focus();
    }
  };

  return (
    <form onSubmit={submitHandler} id="loginForm">
      <h1 className={styles['title']} id="loginFormLabel">
        CareerHub
      </h1>
      <div className={styles['fields']} id="authFieldsDiv">
        <AuthField
          ref={emailInputRef}
          id="email"
          placeholder="Уведіть email"
          type="email"
          isValid={emailIsValid}
          onInputChange={emailChangeHandler}
          onBlur={validatePasswordHandler}
          validationMessage="Перевірте чи ваша пошта не містить невалідних символів"
        >
          <i id="emailIcon" className="far fa-envelope" />
        </AuthField>
        <AuthField
          ref={passwordInputRef}
          id="password"
          placeholder="Уведіть пароль"
          type="password"
          isValid={passwordIsValid}
          isPassword
          onInputChange={passwordChangeHandler}
          onBlur={validateEmailHandler}
          validationMessage="Пароль повинен мати більше 7 символів серед яких: літери верхнього й нижнього регістру, хоча б одна цифра або спеціальний символ"
        >
          <i id="passwordIcon" className="fas fa-key"></i>
        </AuthField>
        <button
          id="submitButton"
          type="submit"
          className={styles['auth-button']}
        >
          Увійти
        </button>
        <button
          id="formSwitchButton"
          onClick={onFormChange}
          className={styles['register-button']}
        >
          Зареєструватися?
        </button>
      </div>
    </form>
  );
};

export default LoginForm;
